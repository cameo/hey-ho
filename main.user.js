// ==UserScript==
// @name         hey-ho
// @namespace    fr.cines
// @version      0.8
// @updateURL    https://git.cines.fr/cameo/hey-ho/raw/master/main.user.js
// @downloadURL  https://git.cines.fr/cameo/hey-ho/raw/master/main.user.js
// @description  try to take over the time, White Snow way
// @author       Victor Cameo Ponz
// @match        https://saas-cines.octime.net/WEOCTIME100/P_ACCUEIL_WE/*
// @grant        none
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @require      http://momentjs.com/downloads/moment.min.js
// ==/UserScript==

function toHHMMSS(duration) {
    if(duration.asMilliseconds() < 0){
        return "-" + toHHMMSS(moment.duration(-duration.asMilliseconds(), 'ms'));
    }
    return (Math.floor(duration.asHours()) + moment.utc(duration.asMilliseconds()).format(":mm")).replace(':', 'h');
}

$(document).ready(function() {
    const now = moment()
    const today_7_30 = moment().hours(7).minutes(30).seconds(0)
    const today_19_30 = moment().hours(19).minutes(30).seconds(0)
    const today_14_30 = moment().hours(14).minutes(30).seconds(0)
    const contract_hours_per_day = moment.duration({hours: 7, minutes: 54})
    const min_out_launch = moment.duration({minutes: 45})
    const extra_activation_threshold = moment.duration({hours: 6})
    const extra_amout = moment.duration({minutes: 20})

    const table_row_patern = '<tr style="background-color: lightgreen;"><td style="width:408px;height:28px;"><div style="float:right;text-align:center;padding-left:5px;padding-right:5px;height:15px;line-height:14px;background-color:#E6EAEE;border-radius:10px;border-width:2px;border-style:solid;border-color:#E6EAEE;">__VALUE__</div><div style="padding-top:0px;padding-bottom:10px;min-height:19px;">__TITLE__ __COMMENT__</div></td></tr>';

    var solde_dc_str = $("div:contains('Solde D/C')").prev().text().trim();
    var solde_dc = moment.duration(solde_dc_str.replace('h', ':')+':00')
    console.log('Got solde DC, str: "'+ solde_dc_str + '", minutes: ' + solde_dc.asMinutes())

    var badgeages_today = []
    $('#CH_VISUBAD option').each(function(idx, el){
        var date_h_action = el.innerHTML.split('&nbsp;')
        var t = moment(date_h_action[0] + " " + date_h_action[1], 'DD/MM/YYYY HH:mm', true)
        if (! now.isSame(t, 'day')) {return}

        badgeages_today.push(t)
    })

    // add a badgeage now if number of badgeage is not pair
    if(badgeages_today.length % 2 === 1){
        console.log("Adding go out today now")
        badgeages_today.unshift(now)
    }

    //Work is accounted between 7h30 and 19h30
    if(badgeages_today[0].isAfter(today_19_30)){
        console.log("Last badgeage after 19h30, replacing with 19h30")
        badgeages_today[0] = today_19_30
    }
    if(badgeages_today[badgeages_today.length - 1].isBefore(today_7_30)){
        console.log("First badgeage before 7h30, replacing with 7h30")
        badgeages_today[badgeages_today.length - 1] = today_7_30
    }
    console.log(badgeages_today)

    var current_time_today = moment.duration(0);
    var current_time_off_today = moment.duration(0);
    for (var i = badgeages_today.length - 1; i > 0; i--) {
            if(i % 2 === 1){// i is a go out
                current_time_today.add(badgeages_today[i-1].diff(badgeages_today[i]))
            } else {
                current_time_off_today.add(badgeages_today[i-1].diff(badgeages_today[i]))
            }
    }
    console.log("Current time in:" + current_time_today.asMinutes())
    console.log("Current time off:" + current_time_off_today.asMinutes())

    var extra_str = '<font color="red">no extra</font>';
    var remaining_lunch_str = '<font color="grey">pause non prise</font>';

    // remove midday pause it has not been taken
    if (now.isAfter(today_14_30)){
        if(current_time_off_today.asMinutes() < min_out_launch.asMinutes()){
            var remaining_lunch_minutes = min_out_launch.asMinutes() - current_time_off_today.asMinutes()
            console.log('Pause duration < ' + min_out_launch.asMinutes() + ' min, removing')
            current_time_today.subtract(remaining_lunch_minutes, 'minutes')
            remaining_lunch_str = '<font color="red">' + remaining_lunch_minutes + 'm pause restante</font>'
        } else {
            remaining_lunch_str = '<font color="green">pause prise</font>';
        }
    }

    // add extra
    if (current_time_today.asMinutes() >= extra_activation_threshold.asMinutes()) {
        console.log('Worked more than 6h, add 20 extra minutes')
        extra_str = '<font color="green">20m extra</font>';
        current_time_today.add(extra_amout);
    }

    var current_time_today_dc = current_time_today.clone()
    current_time_today_dc.subtract(contract_hours_per_day)
    var $table_dc = $("div:contains('Solde D/C')").last().closest('table');
    $("div:contains('Débit/Crédit quotidien')").css('padding-bottom', '10px');
    $table_dc.append(
        table_row_patern.replace("__TITLE__", "Quotidien avec sortie immédiate")
                        .replace("__COMMENT__", extra_str + "/" + remaining_lunch_str)
                        .replace("__VALUE__", toHHMMSS(current_time_today)));
    $table_dc.append(
        table_row_patern.replace("__TITLE__", "D/C Quotidien avec sortie immédiate")
                        .replace("__COMMENT__", "")
                        .replace("__VALUE__", toHHMMSS(current_time_today_dc)));
    $table_dc.append(
        table_row_patern.replace("__TITLE__", "Départ pour journée complète")
                        .replace("__COMMENT__", "")
                        .replace("__VALUE__", now.subtract(current_time_today_dc).format("HH:mm")));
    $table_dc.append(
        table_row_patern.replace("__TITLE__", "Solde D/C avec sortie immédiate")
                        .replace("__COMMENT__", "")
                        .replace("__VALUE__", toHHMMSS(current_time_today_dc.add(solde_dc))));
});
