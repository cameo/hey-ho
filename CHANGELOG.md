# Changelog

## v0.6 - Unrealeased
 - NEW Changelog #9
 - IMPROVE Make the script octime generic (no domain specification) #11

## v0.5 - 2018-06-19
 - NEW Exclude working hour before 7:30 and after 19h30 #7
 - FIX spelling #4
 - IMPROVE documentation on Grease-Tampermonkey #5

## v0.4 - 2018-06-05
 - FIX "Pause prise" is triggered on the morning while it should not #1
 - FIX typo in the update URL

## v0.3 - 2018-06-01
 - NEW update through X-monkey URL update field
 - IMPROVE documentation: install procedure and showcase

## v0.2 - 2018-06-01
 - FIX negative time is 1h wrong
 - IMPROVE "pause" display to trigger only after 14h30

## v0.1 - 2018-06-01
 - Initial release.
