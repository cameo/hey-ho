# Install

 * Install a userscript extention on your browser
   * on Chrome with [tampermonkey](http://tampermonkey.net/)
   * on FireFox with [greasemonkey](https://www.greasespot.net/) or [tampermonkey](http://tampermonkey.net/)
 * install this user script by clicking: [here](https://git.cines.fr/cameo/hey-ho/raw/master/main.user.js)

# Usage

Nothing to do, just go to the badgeuse website and enjoy new green rows on your today balance ! 

Go from:

![Boring calculations](images/before.png)

To:

![Not spending the day on the OCtime website](images/after.png)

# Feature and bug reports welcomed

Please [open issues](https://git.cines.fr/cameo/hey-ho/issues/new?issue), to discuss, ask for features, report bugs...

See also open [bug](https://git.cines.fr/cameo/hey-ho/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=bug) and [feature](https://git.cines.fr/cameo/hey-ho/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=feature) lists.


# Snow White


[!["Hé oh, hé oh, on rentre du boulot"](http://img.youtube.com/vi/IlFWrDp-dY8/0.jpg)](http://www.youtube.com/watch?v=IlFWrDp-dY8 ""Hé oh, hé oh, on rentre du boulot"")
